<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">User Registration</h3>
    </div>

    <div class="panel-body">
        <form action="<?php echo ROOT_URL; ?>users/register" method="post">

            <div class="form-group">
                <label for="">Name</label>
                <input type="text" name="user_name" class="form-control">
            </div>

            <div class="form-group">
                <label for="">Email</label>
                <input type="email" name="email" class="form-control">
            </div>

            <div class="form-group">
                <label for="">Password</label>
                <input type="password" name="password" class="form-control">
            </div>

            <input type="submit" class="btn btn-primary" name="submit" value="Submit">

        </form>
    </div>
</div>