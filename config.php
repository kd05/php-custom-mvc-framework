<?php

// Define DB Params
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASS", "password");
define("DB_NAME", "shareboard");

// Define URL
define("ROOT_PATH", "/php.mvc/");
define("ROOT_URL", "http://php.mvc/");