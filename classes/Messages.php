<?php
class Messages{
    public static function setMsg($text,$type){
        if($type == "success"){
            $_SESSION['successMsg'] = $text;
        }
        if($type == "error"){
            $_SESSION['errorMsg'] = $text;
        }
    }


    public static function display(){
        if(isset($_SESSION['successMsg'])){
            echo "<div class='alert alert-success'>".$_SESSION['successMsg']."</div>";
            unset($_SESSION['successMsg']);
        }
        if(isset($_SESSION['errorMsg'])){
            echo "<div class='alert alert-danger'>".$_SESSION['errorMsg']."</div>";
            unset($_SESSION['errorMsg']);
        }
    }

}