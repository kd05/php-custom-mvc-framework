<?php
class UserModel extends Model{

	public function register(){
		$post = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);
		$password = md5($post['password']);
		if($post['submit']){

			if($post['user_name'] == "" || $post['email'] == "" || $post['password'] == "" ){
				Messages::setMsg("Please enter all the fields!","error");
				return;
			}

			$this->query("INSERT INTO users (name, email, password) VALUES (:name, :email, :password)");
			$this->bind(":name",$post['user_name']);
			$this->bind(":email",$post['email']);
			$this->bind(":password",$password);
			$this->execute();

			if($this->lastInsertId()){
				header("location: ".ROOT_URL."users/login");
			}
		}
	}


	public function login(){
		$post = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);

		$password = md5($post['password']);
		if($post['submit']){
			$query = "select * from users where email = :email AND password = :password";
			$this->query($query);
			$this->bind(":email",$post['email']);
			$this->bind(":password",$password);
			$row = $this->single();
			if($row){
				echo "Logged In";
				$_SESSION['is_logged_in'] = true;
				$_SESSION['user_data']	= array(
					"id"	=>	$row['id'],
					"name"	=>	$row['name'],
					"email" =>  $row['email']
				);
				header("location: ".ROOT_URL."shares");
			} else {
				Messages::setMsg("Invalid Credentials","error");
				//				echo "Incorrect Credentials";
			}
		}
	}

	public function logout(){
		unset($_SESSION['is_logged_in']);
		unset($_SESSION['user_data']);
		session_destroy();
		header("location: ".ROOT_URL."users/login");
	}

}